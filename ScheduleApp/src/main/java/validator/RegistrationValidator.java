package validator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.ws.rs.core.Response;

import data.UserRepository;
import model.User;

public class RegistrationValidator {

	@Inject
	private UserRepository users;

	@Inject
	private Validator validator;

	public void validateUser(User user) throws ConstraintViolationException, ValidationException {

		Set<ConstraintViolation<User>> violations = validator.validate(user);

		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
		}
		if (emailAlreadyExists(user.getEmail())) {
			throw new ValidationException("Unique Email Violation");
		}
		if (userNameAlreadyExists(user.getUserName())) {
			throw new ValidationException("Unique Username Violation");
		}

	}

	public Response.ResponseBuilder createViolationResponse(Set<ConstraintViolation<?>> violations) {

		Map<String, String> responseObj = new HashMap<>();

		for (ConstraintViolation<?> violation : violations) {
			responseObj.put(violation.getPropertyPath().toString(), violation.getMessage());
		}

		return Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
	}

	private boolean emailAlreadyExists(String email) {
		User user = null;
		try {
			user = users.findByEmail(email);
		} catch (NoResultException e) {
			// Ignore
		}
		return user != null;
	}

	private boolean userNameAlreadyExists(String userName) {
		User user = null;
		try {
			user = users.findByUserName(userName);
		} catch (NoResultException e) {
			// Ignore
		}
		return user != null;
	}
}
