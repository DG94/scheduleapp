package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "credentials", uniqueConstraints = @UniqueConstraint(columnNames = { "user_id", "token" }))
@NamedQueries({
	@NamedQuery(name="Credentials.findById", query="SELECT c FROM Credentials c WHERE c.user.id =:id"),
	@NamedQuery(name="Credentials.findByToken", query="SELECT c FROM Credentials c WHERE c.token =:token")
})
@XmlRootElement
public class Credentials implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	@NotNull
	private User user;

	@Column(name = "token", nullable = false)
	@NotNull
	private String token;
	
	Credentials() {
	}

	public Credentials(User user, String token) {
		this.user = user;
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
