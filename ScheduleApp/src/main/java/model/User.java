package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import listener.RegistrationListener;

@Entity
@EntityListeners(RegistrationListener.class)
@Table(name = "users")
@NamedQueries({
	@NamedQuery(name="User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
	@NamedQuery(name="User.findByUserName", query = "SELECT u FROM User u WHERE u.userName = :userName"),
	@NamedQuery(name="User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email")
})
@XmlRootElement
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique= true, nullable = false)
	private Long id;
	
	
	@Column(name = "username", unique = true, nullable = false)
	@NotNull
	@Size(min = 2, max = 45)
	private String userName;
	
	@Column(unique = true, nullable = false)
	@NotNull
	@Email
	@Size(max = 60)
	private String email;
	
	@Column(name = "password", nullable = false)
	private String encryptedPassword;
	
	@Column(name = "is_active", nullable = false)
	private Boolean isActive = true;
	
	@Transient
	@NotNull
	transient private String password;
	
	
	User(){}
	
	public User(String userName, String email, String password){
		this.userName = userName;
		this.email = email;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

}
