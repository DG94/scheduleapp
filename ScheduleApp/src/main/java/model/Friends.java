package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "friends", uniqueConstraints = @UniqueConstraint(columnNames = { "sender_id", "receiver_id" }))
@XmlRootElement								
public class Friends implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "friends_id", unique = true, nullable = false)
	@NotNull
	private Long id;

	@ManyToOne
	@JoinColumn(name = "sender_id", foreignKey = @ForeignKey(name = "sender_id_fk"), nullable= false)
	@NotNull
	private User sender;

	@ManyToOne
	@JoinColumn(name = "receiver_id", foreignKey = @ForeignKey(name = "receiver_id_fk"), nullable= false)
	@NotNull
	private User receiver;
	
	
	@Column(name = "is_friend", nullable = false)
	@NotNull
	private boolean isFriend = false;
	
	Friends(){}
	
	public Friends(User sender, User receiver){
		this.sender = sender;
		this.receiver = receiver;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

}
