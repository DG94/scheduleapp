package security;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import data.CredentialsRepository;
import service.TokenService;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthFilter implements ContainerRequestFilter {
	
	@Inject
	TokenService ts;
	
	@Inject
	CredentialsRepository cr;

	@Override
	public void filter(ContainerRequestContext reqContext) throws IOException {
		String authHeader = reqContext.getHeaderString(HttpHeaders.AUTHORIZATION);

		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			throw new NotAuthorizedException("Authorization header must be provided");

		}
		String token = authHeader.substring("Bearer".length()).trim();

		try {
			if(cr.findByToken(token)==null)
				reqContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
			
			ts.validateToken(token);

		} catch (Exception e) {
			reqContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}
}
