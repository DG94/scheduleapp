package data;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import model.Credentials;
import model.User;

@Stateless
public class CredentialsRepository {

	@PersistenceContext
	private EntityManager em;

	public void add(Credentials credentials) {
		em.persist(credentials);
	}

	public Credentials findById(Long id) {
		TypedQuery<Credentials> query = em.createNamedQuery("Credentials.findById", Credentials.class);
		query.setParameter("id", id);
		List<Credentials> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		else
			return resultList.get(0);
	}
	public Credentials findByToken(String token){
		TypedQuery<Credentials> query = em.createNamedQuery("Credentials.findByToken", Credentials.class);
		query.setParameter("token",token );
		List<Credentials> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		else
			return resultList.get(0);	
	}

	public void update(User user, String token) {
		Credentials credentials = em.find(Credentials.class, user.getId());
		if (credentials == null) {
			throw new IllegalArgumentException();
		}
		credentials = new Credentials(user, token);
	}
}
