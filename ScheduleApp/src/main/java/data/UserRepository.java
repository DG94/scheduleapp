package data;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import model.User;

@Stateless
public class UserRepository {

	@PersistenceContext
	private EntityManager em;

	public void add(User user) {
		em.persist(user);
	}

	public User findByUserName(String userName) {
		TypedQuery<User> query = em.createNamedQuery("User.findByUserName", User.class);
		query.setParameter("userName", userName);
		List<User> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		else
			return resultList.get(0);
	}

	public User findByEmail(String email) {
		TypedQuery<User> query = em.createNamedQuery("User.findByEmail", User.class);
		query.setParameter("email", email);
		List<User> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		else
			return resultList.get(0);
	}
}
