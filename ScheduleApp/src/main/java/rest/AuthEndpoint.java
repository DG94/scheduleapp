package rest;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.mindrot.jbcrypt.BCrypt;

import data.CredentialsRepository;
import data.UserRepository;
import model.Credentials;
import model.User;
import service.TokenService;

@RequestScoped
@Path("/auth")
public class AuthEndpoint {
	private static final Logger LOGGER = Logger.getLogger(AuthEndpoint.class);

	@Inject
	private UserRepository users;
	@Inject
	private CredentialsRepository cr;

	@Inject
	private TokenService ts;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(User user) {
		Response.ResponseBuilder builder = null;
		try {
			if (user.getUserName() != null && user.getPassword() != null) {
				User authUser = users.findByUserName(user.getUserName());
				if (authUser != null && BCrypt.checkpw(user.getPassword(), authUser.getEncryptedPassword())) {
					String token = ts.generateToken(authUser.getId());
					Credentials credentials = new Credentials(authUser, token);
					if (cr.findById(authUser.getId()) == null) {
						cr.add(credentials);
					} else {

						cr.update(authUser, token);
					}
					Map<String, String> responseObj = new HashMap<>();
					responseObj.put("token", token);
					responseObj.put("id", authUser.getId().toString());
					builder = Response.ok().entity(responseObj);
					LOGGER.info(authUser.getUserName() + "logged in");
				} else {
					Map<String, String> responseObj = new HashMap<>();
					responseObj.put("error", "Wrong username or password!");
					builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
				}
			}
		} catch (Exception e) {
			builder = Response.status(Response.Status.UNAUTHORIZED);
		}
		return builder.build();
	}

}
