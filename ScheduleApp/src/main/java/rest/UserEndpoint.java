package rest;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import data.UserRepository;
import model.User;
import validator.RegistrationValidator;
import org.jboss.logging.Logger;

@RequestScoped
@Path("/users")
public class UserEndpoint {
	private static final Logger LOGGER = Logger.getLogger(UserEndpoint.class);

	@Inject
	private UserRepository users;

	@Inject
	private RegistrationValidator rv;

	// Rejestracja
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(User user) {
		Response.ResponseBuilder builder = null;
		try {
			if (user != null && user.getId() == null) {
				// Walidacja uzytkownika
				rv.validateUser(user);
				users.add(user);
				LOGGER.info("New user registered: " + user.getUserName() + ", " + user.getEmail());
				// Jest ok
				builder = Response.status(Status.CREATED);
			} else if (user == null)
				// Uzytkownik wyslal zly JSON -> wpisa� r�cznie userID lub
				// wys�a� pustego JSONa
				builder = Response.status(Status.BAD_REQUEST);
			else if (user.getId() != null) {
				LOGGER.warn("Someone's trying send ID to DB manually");
				builder = Response.status(Status.BAD_REQUEST);
			} else
				builder = Response.status(Status.BAD_REQUEST);

		} catch (ConstraintViolationException cve) {
			// B�edy wygenerowane przez BeanValidation
			builder = rv.createViolationResponse(cve.getConstraintViolations());
		} catch (ValidationException ve) {
			// B�edy Unique
			Map<String, String> responseObj = new HashMap<>();
			if (ve.getMessage().equals("Unique Email Violation"))
				responseObj.put("error", "Email taken");

			if (ve.getMessage().equals("Unique Username Violation"))
				responseObj.put("error", "Username taken");

			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);

		} catch (Exception e) {
			// Inne b��dy
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

}
