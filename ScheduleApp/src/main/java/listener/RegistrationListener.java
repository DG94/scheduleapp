package listener;

import javax.persistence.PrePersist;

import org.mindrot.jbcrypt.BCrypt;

import model.User;

public class RegistrationListener {
	
	@PrePersist
	public void encryptPassword(User user){
		user.setEncryptedPassword(null);
		if(user.getPassword() !=null){
			user.setEncryptedPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
		}
	}
}
