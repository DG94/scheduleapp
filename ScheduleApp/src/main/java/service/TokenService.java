package service;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;

import data.UserRepository;

public class TokenService {
	
	@Inject
	private UserRepository users;

	public String generateToken(Long userId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException, JoseException {

		// Key generation with hashed simple string
		Key key = getKey();

		// Creating claims
		JwtClaims claims = new JwtClaims();
		claims.setIssuer("DG94");
		claims.setExpirationTimeMinutesInTheFuture(1440);
		claims.setGeneratedJwtId();
		claims.setIssuedAtToNow();
		claims.setNotBeforeMinutesInThePast(10);
		claims.setClaim("id", userId);

		// Adding signature
		JsonWebSignature jws = new JsonWebSignature();
		jws.setPayload(claims.toJson());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
		jws.setKey(key);

		String token = jws.getCompactSerialization();
		
		return token;

	}
	
	public void validateToken(String token) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidJwtException{
		Key key = getKey();
		
		JwtConsumer jwtConsumer = new JwtConsumerBuilder()
				.setExpectedIssuer("DG94")
				.setRequireExpirationTime()
				.setAllowedClockSkewInSeconds(900)
				.setVerificationKey(key)
				.build();
		jwtConsumer.processToClaims(token);
	}

	private Key getKey() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		String secret = "secret";
		md.update(secret.getBytes("UTF-8"));
		Key key = new HmacKey(md.digest());
		return key;

	}

}
