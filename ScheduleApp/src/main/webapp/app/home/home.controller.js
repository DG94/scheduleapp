(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);
    
    function HomeController(){
    	var vm = this;
    	
    	vm.message = "Welcome to ScheduleApp! Sign up if you dont have an account yet";
    }
})();