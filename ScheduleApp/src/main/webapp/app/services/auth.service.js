(function(){
	'use strict';
	
	angular
		.module('app')
		.factory('AuthService',AuthService);
	
	AuthService.$inject = ['$resource'];
	function AuthService ($resource){
		return $resource('api/auth');
	}
})();