(function(){
	'use strict';
	
	angular
		.module('app')
		.factory('UserService',UserService);
	
	UserService.$inject = ['$resource'];
	function UserService($resource){
		//return $resource('api/users/:id',{id: '@id'});
		return $resource('api/users');
	}
})();