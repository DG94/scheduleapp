(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);
    
    LoginController.$inject = ['AuthService','$location', '$rootScope','Flash'];
    function LoginController(AuthService, $location, $rootScope, Flash){
    	var vm = this;
    	
    	vm.login = login;
    	
    	function login(){
    		AuthService.save(vm.user).$promise.then(authSuccess, authError);
    		
    		function authSuccess(){
    			Flash.create('success','Login successfully!');
    			$location.path('/main');
    		}
    		function authError(response){
    			Flash.create('danger',respons.data.error);
    		}
    	}
    }
})();