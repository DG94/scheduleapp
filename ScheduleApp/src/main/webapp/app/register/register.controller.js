(function() {
	'use strict';

	angular
		.module('app')
		.controller('RegisterController', RegisterController);

	RegisterController.$inject = [ 'UserService', '$location', '$rootScope','Flash' ];
	function RegisterController(UserService, $location, $rootScope, Flash) {
		var vm = this;
		
		vm.register = register;
		
		function register(){
			UserService.save(vm.user).$promise.then(userRegistered,registrationError);
			
			function userRegistered(){
				Flash.create('success','Registered successfully');
				$location.path('/login');
			}
			function registrationError(response){
				Flash.create('danger',response.data.error);
			
			}
		}
		
	}
})();